import {Component, OnInit} from '@angular/core';
import {CellComponent} from './cell/cell.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

  positions: number[][] = [];
  currentActionIndex: number;
  result: string;
  winner: boolean;

  constructor() { }

  ngOnInit(): any {
    this.initBoard();
  }

  checkCell(cellComponent: CellComponent) {
    if (!this.winner) {
      if (cellComponent.owner != -1) {
          this.result = "Cell already choosen";
      } else {
        cellComponent.owner = this.currentActionIndex % 2;
        this.currentActionIndex++;
        this.positions[ cellComponent.x ] [ cellComponent.y ] = cellComponent.owner;
        if (cellComponent.owner % 2 !== 0) {
            this.result = "Players 1 Turn";
        } else {
            this.result = "Players 2 Turn";
        }
      }
    }

    this.checkBoard();
  }

  checkBoard() {
    if (this.currentActionIndex == 9) {
      this.winner = true;
      this.result = 'Draw game!!';
    }

    this.checkRows();
    this.checkColumns();
    this.checkDiagonals();
  }

  private checkRows() {
    [0, 1, 2].forEach((row) => {
      this.checkWinner(this.positions[row].reduce((acc, current) => acc + current, 0));
    })
  }

  private checkColumns() {
    [0, 1, 2].forEach((col) => {
      this.checkWinner(this.positions.map(function(value, index) { return value[col]; }).reduce((acc, current) => acc + current, 0));
    });
  }

  private checkDiagonals() {
    if (this.positions[0][0] != -5 &&  this.positions[0][0] == this.positions[1][1] && this.positions[1][1] == this.positions[2][2]) {
      this.result = this.positions[0][0] % 2 == 0 ? 'Player 1 Winner!!' : 'Player 2 Winner!!';
      this.winner = true;
    }
    if (this.positions[2][0] != -5 &&  this.positions[2][0] == this.positions[1][1] && this.positions[1][1] == this.positions[0][2]) {
      this.result = this.positions[2][0] % 2 == 0 ? 'Player 1 Winner!!' : 'Player 2 Winner!!';
      this.winner = true;
    }
  }

  private checkWinner(sum: number) {
    if (sum == 0) {
      this.result = 'Player 1 Winner!!';
      this.winner = true;
    } else if (sum == 3) {
      this.result = 'Player 2 Winner!!';
      this.winner = true;
    }
  }

  private initBoard() {
    this.result = "Players 1 Turn";
    this.currentActionIndex = 0;
    [0,1,2].forEach((i) => {
      this.positions[i] = [];
      [0,1,2].forEach((j) => this.positions[i][j] = -5);
    });
  }

}

import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'cell',
  template: `
        <div [ngClass]="{ 'empty': owner == -1, 'player1': owner == 0, 'player2': owner == 1}"
             (click)="emitComponent()"></div>
    `,
})
export class CellComponent {

  @Input('cell-x') x: number;
  @Input('cell-y') y: number;
  @Output() makeMove: EventEmitter<any> = new EventEmitter();
  owner: number;

  ngOnInit():any {
    this.owner = -1;
  }

  emitComponent() {
    this.makeMove.emit(this);
  }

}
